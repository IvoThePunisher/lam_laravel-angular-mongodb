import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  public href: string= "";

  constructor(private router: Router) { }

  home(){  
    return this.href = this.router.url === '/' ? true : false;
  }
  ngOnInit(): void {   
    console.log('la ruta actual es: ' + this.router.url + '----' + this.href);
  }

}
