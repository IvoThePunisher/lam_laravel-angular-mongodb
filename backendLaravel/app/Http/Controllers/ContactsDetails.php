<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contacts;

class ContactsDetails extends Controller
{
    public function listContacts(){        
        return Contacts::contacts();
        /*return view('contacts',  ['name' => 'ContactsDetails', 'active' => array("","active","",""),
                                  'data' => Contacts::listContacts
        ]);*/
    }
}
