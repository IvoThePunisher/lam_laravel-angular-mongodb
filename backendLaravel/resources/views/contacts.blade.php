@extends('master')

@section('title', 'LAM - Contactos')

@section('body_content')
<?php $contacts_list = json_decode($contacts, true); 
//var_dump($contacts_list[0]);
foreach($contacts_list as $list => $value){
    $keys=array_keys($value);
}
    unset($keys[0]);
?>
    <h1>Contacts list from MongoDB</h1>
    <table class="table table-hover table-sm">
        <thead>
             <tr>
                 @foreach($keys as $key)
                 <th> {{ $key }} </th>
                 @endforeach
            </tr>       
        </thead>
        <tbody>
            @foreach($contacts_list as $contact)
                <tr>
                    <td> {{ $contact['name'] }} </td>
                    <td> {{ $contact['age'] }} </td>
                    <td> {{ $contact['location'] }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
<br>
<p>Made with Laravel blade template.</p>
@stop