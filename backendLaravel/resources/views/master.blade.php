<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title')</title>

    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">
    <link href="{{ asset('../resources/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('../resources/css/style.css') }}" rel="stylesheet">
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>

    @yield('extras')

  </head>
  <body>

    <div class="container-fluid">
	<div class="row">		
        <div class="col-md-12">
			<div class="row nav_bar">
                <div class="col-md-2">
                    <img class="icon_image" src="{{ asset('../resources/images/laravel.png') }}">
                </div>
                <div class="col-md-10">
                    <nav class="navbar bg-green">
                        <ul class="nav">
                            <li class="nav-item">
                                <a class="nav-link active" href="#">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Profile</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link disabled" href="#">Messages</a>
                            </li>
                            <li class="nav-item dropdown ml-md-auto">
                                 <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown">Dropdown link</a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                     <a class="dropdown-item" href="#">Action</a> <a class="dropdown-item" href="#">Another action</a> <a class="dropdown-item" href="#">Something else here</a>
                                    <div class="dropdown-divider">
                                    </div> <a class="dropdown-item" href="#">Separated link</a>
                                </div>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">  
                </div>
                <div class="col-md-8">  
                    <img class="back_image" src="{{ asset('../resources/images/fondoTecnologico.jpg') }}">
                </div>
                <div class="col-md-2">  
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">  
                </div>
                <div class="col-md-8 body_content">
                @section('body_content')
                    <div class="tabbable" id="tabs-530180">
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a class="nav-link active" href="#tab1" data-toggle="tab">Section 1</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#tab2" data-toggle="tab">Section 2</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="panel-704914">
                                <p>
                                    I'm in Section 1.
                                </p>
                            </div>
                            <div class="tab-pane" id="tab2">
                                <p>
                                    Howdy, I'm in Section 2.
                                </p>
                            </div>
                        </div>
                    </div>
                    <dl>
                        <dt>
                            Description lists
                        </dt>
                        <dd>
                            A description list is perfect for defining terms.
                        </dd>
                        <dt>
                            Euismod
                        </dt>
                        <dd>
                            Vestibulum id ligula porta felis euismod semper eget lacinia odio sem nec elit.
                        </dd>
                        <dd>
                            Donec id elit non mi porta gravida at eget metus.
                        </dd>
                        <dt>
                            Malesuada porta
                        </dt>
                        <dd>
                            Etiam porta sem malesuada magna mollis euismod.
                        </dd>
                        <dt>
                            Felis euismod semper eget lacinia
                        </dt>
                        <dd>
                            Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
                        </dd>
                    </dl>
                    <div class="media">
                        <img class="mr-3" alt="Bootstrap Media Preview" src="https://www.layoutit.com/img/sports-q-c-64-64-8.jpg">
                        <div class="media-body">
                            <h5 class="mt-0">
                                Nested media heading
                            </h5> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.
                            <div class="media mt-3">
                                 <a class="pr-3" href="#"><img alt="Bootstrap Media Another Preview" src="https://www.layoutit.com/img/sports-q-c-64-64-2.jpg"></a>
                                <div class="media-body">
                                    <h5 class="mt-0">
                                        Nested media heading
                                    </h5> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.
                                </div>
                            </div>
                        </div>
                    </div>
                @show
                </div>
                <div class="col-md-2">  
                </div>
            </div>
        </div>
	</div>
</div>

    <script src="../js/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/scripts.js"></script>
  </body>
</html>